package id.haadii.binar.binarchapter6

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by nurrahmanhaadii on 26,May,2022
 */
@RunWith(AndroidJUnit4::class)
class MainActivityAndroidTest {

    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun day5Fragment() {
        onView(withId(R.id.btnSet)).check(matches(isDisplayed()))
        onView(withId(R.id.btnIncrease)).perform(click())
        onView(withId(R.id.tvValue)).check(matches(withSubstring("11")))
        onView(withId(R.id.btnDecrease)).perform(click())
        onView(withId(R.id.tvValue)).check(matches(withSubstring("10")))
    }
}