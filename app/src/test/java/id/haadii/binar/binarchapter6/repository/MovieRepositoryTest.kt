package id.haadii.binar.binarchapter6.repository

import id.haadii.binar.binarchapter6.model.MovieResponse
import id.haadii.binar.binarchapter6.model.Resource
import id.haadii.binar.binarchapter6.service.ApiService
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

/**
 * Created by nurrahmanhaadii on 25,May,2022
 */
class MovieRepositoryTest {

    private val apiService: ApiService = mockk()
    lateinit var repository: MovieRepository

    @Before
    fun setUp() {
        repository = MovieRepository(apiService)
    }

    @Test
    fun getMovie() {
    }

    @Test
    fun getUsers(): Unit = runBlocking {
        // Mocking (GIVEN)
        val respAllCar = mockk<MovieResponse>()

        every {
            runBlocking {
                apiService.getAllMovie("498549d41b041783631ec3d45977d4c2")
            }
        } returns respAllCar

        // (THEN)
        verify {
            runBlocking { apiService.getAllMovie("498549d41b041783631ec3d45977d4c2") }
        }
    }

}