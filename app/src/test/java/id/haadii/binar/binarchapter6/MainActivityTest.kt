package id.haadii.binar.binarchapter6

import android.content.Context
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import org.mockito.Mockito.mock

/**
 * Created by nurrahmanhaadii on 26,May,2022
 */
class MainActivityTest {
    lateinit var data: Data
    lateinit var name: Name

    @Before
    fun setup() {
        val context: Context = mock(Context::class.java)
        val mContextMock = mockk<Context>()

        data = Data()
        name = Name(context, data)
    }

    @Test
    fun getNameD() {
        val result = name.getNameFromRes()
        assertEquals("nurrahman", result)
    }
}