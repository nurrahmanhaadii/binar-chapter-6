package id.haadii.binar.binarchapter6.helper

import id.haadii.binar.binarchapter6.toDate
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import java.util.*

/**
 * Created by nurrahmanhaadii on 26,May,2022
 */
class StringHelperTest {
    lateinit var helper: StringHelper

    @Before
    fun setup() {
        helper = StringHelper()
    }

    @Test
    fun isPositiveNumber() {
        val result = helper.isPositiveNumber(2)
        assertTrue(result)
    }

    @Test
    fun toCapital() {
        val result = helper.toCapital("hadi")
        assertEquals("Hadi", result)
    }

    @Test
    fun doSomething() {
        val today = Date()

        val cal = Calendar.getInstance()
//        cal.add(Calendar.DATE, 1)
        val tomorrow = cal.time

        cal.add(Calendar.DAY_OF_YEAR, -1)
        val yesterday = cal.time

        val result = helper.doSomething(today, yesterday)
        assertEquals("Yesterday", result)
    }

    @Test
    fun toDate() {
        val result = "2022-05-30".toDate()
        assertEquals("Mei Senin, 2022", result)
    }
}