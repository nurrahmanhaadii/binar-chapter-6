package id.haadii.binar.binarchapter6.helper

import id.haadii.binar.binarchapter6.appModule
import org.junit.Assert.*

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject

/**
 * Created by nurrahmanhaadii on 26,May,2022
 */
class StringHelperTest2: KoinTest {

    val helper by inject<StringHelper>()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(appModule)
    }

    @Test
    fun isPositiveNumber() {
        val result = helper.isPositiveNumber(5)
        assertTrue(result)
    }

    @Test
    fun toCapital() {
    }
}