package id.haadii.binar.binarchapter6.day6

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.haadii.binar.binarchapter6.R
import id.haadii.binar.binarchapter6.databinding.FragmentPdfHanldeBinding

class PdfHandleFragment : Fragment() {
    private var _binding: FragmentPdfHanldeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPdfHanldeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openWebView()
        openFromAsset()
        openFromStorage()
        openFromInternet()
    }

    private fun openWebView() {
        binding.buttonWebView.setOnClickListener {
            findNavController().navigate(R.id.action_pdfHandleFragment_to_webViewFragment)
        }
    }

    private fun openFromAsset() {
        binding.buttonAssets.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("KEY_OPEN_FROM", 1)
            findNavController().navigate(R.id.action_pdfHandleFragment_to_pdfViewFragment, bundle)
        }
    }

    private fun openFromStorage() {
        binding.buttonStorage.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("KEY_OPEN_FROM", 2)
            findNavController().navigate(R.id.action_pdfHandleFragment_to_pdfViewFragment, bundle)
        }
    }

    private fun openFromInternet() {
        binding.buttonInternet.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("KEY_OPEN_FROM", 3)
            findNavController().navigate(R.id.action_pdfHandleFragment_to_pdfViewFragment, bundle)
        }
    }

}