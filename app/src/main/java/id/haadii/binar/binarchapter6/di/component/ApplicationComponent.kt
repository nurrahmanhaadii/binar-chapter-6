package id.haadii.binar.binarchapter6.di.component

import android.app.Activity
import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import id.haadii.binar.binarchapter6.App
import id.haadii.binar.binarchapter6.MainActivity
import id.haadii.binar.binarchapter6.day5.Day5Fragment
import id.haadii.binar.binarchapter6.day5.Day5ViewModel
import id.haadii.binar.binarchapter6.day6.Day6Fragment
import id.haadii.binar.binarchapter6.day9.Day9Fragment
import id.haadii.binar.binarchapter6.di.module.ActivityModule
import id.haadii.binar.binarchapter6.di.module.ApplicationModule
import id.haadii.binar.binarchapter6.di.module.FragmentModule
import id.haadii.binar.binarchapter6.di.module.ViewModelModule
import javax.inject.Singleton


/**
 * Created by nurrahmanhaadii on 24,May,2022
 */
@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ActivityModule::class,
        FragmentModule::class,
        AndroidInjectionModule::class
    ]
)
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

    fun inject(app: App)
}