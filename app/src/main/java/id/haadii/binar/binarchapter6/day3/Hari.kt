package id.haadii.binar.binarchapter6.day3


/**
 * Created by nurrahmanhaadii on 27,April,2022
 */
enum class Hari(val code: Int) {
    SENIN(1),
    SELASA(2),
    RABU(3),
    KAMIS(4),
    JUMAT(5),
    SABTU(6),
    MINGGU(7)
}

enum class Gender {
    LAKI_LAKI, PEREMPUAN
}