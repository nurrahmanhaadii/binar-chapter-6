package id.haadii.binar.binarchapter6.day3


/**
 * Created by nurrahmanhaadii on 27,April,2022
 */
class Day3Presenter(private val viewInterface: ViewInterface) : PresenterInterface {
    override fun getData(data: String) {
        viewInterface.showData(data)
    }
}