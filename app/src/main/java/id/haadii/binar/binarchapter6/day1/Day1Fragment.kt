package id.haadii.binar.binarchapter6.day1

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import id.haadii.binar.binarchapter6.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class Day1Fragment : Fragment() {

    private lateinit var tvText: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_day1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvText = view.findViewById(R.id.tv_text)
        onThreadClicked(view)
        onHandlerClicked(view)
        CoroutineScope(Dispatchers.IO).launch {

        }

        GlobalScope.launch {

        }
    }

    private fun onThreadClicked(view: View) {
        val btnThread = view.findViewById<Button>(R.id.btn_thread)
        btnThread.setOnClickListener {
            // buat thread baru di luar Main Thread
            Thread {
                // tidak boleh memanggil ui toolkit di thread yg berbeda dari main thread

                // run on ui thread hanya milik activity
                requireActivity().runOnUiThread {
                }

                tvText.post {

                }

                tvText.postDelayed({
                    tvText.text = "run on ui thread"
                },
                1000)
            }.start()
        }
    }

    val handler = object:  Handler(Looper.getMainLooper()) {

        override fun handleMessage(msg: Message) {

            val message = msg.obj as String

            tvText.text = message

        }

    }

    private fun onHandlerClicked(view: View) {
        val btnHandler = view.findViewById<Button>(R.id.btn_handler)
        btnHandler.setOnClickListener {
            Thread {
                val msg = Message.obtain()
                msg.obj = "run on handler"
                handler.sendMessage(msg)
//            handler.sendMessageDelayed(msg, 1000)
            }.start()

            Thread {

            }.start()

        }
    }

}