package id.haadii.binar.binarchapter6.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import id.haadii.binar.binarchapter6.day5.Day5ViewModel
import id.haadii.binar.binarchapter6.helper.ViewModelFactory
import id.haadii.binar.binarchapter6.helper.ViewModelKey
import javax.inject.Inject


/**
 * Created by nurrahmanhaadii on 25,May,2022
 */
@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(Day5ViewModel::class)
    abstract fun sampleViewModel(viewModel: Day5ViewModel): ViewModel
}