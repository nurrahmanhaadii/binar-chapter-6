package id.haadii.binar.binarchapter6.day2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage

class AirPlaneReceiver : BroadcastReceiver() {
    val intentBroadcast = Intent("id.haadii.chapter6")

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val extras = intent.extras
        if (extras != null) {
            intentBroadcast.putExtra("message", "messageText")
            context?.sendBroadcast(intent)
        }
    }
}