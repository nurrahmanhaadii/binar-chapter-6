package id.haadii.binar.binarchapter6

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@BaseApp)
            modules(listOf(appModule))
        }
    }

    // tempat kita narok semua dependencies yg di butuhkan
    private val appModule = module {
        single { Data() }

        factory { Name(androidContext(), get()) }
    }
}