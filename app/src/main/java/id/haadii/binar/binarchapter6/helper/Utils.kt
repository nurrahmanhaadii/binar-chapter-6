package id.haadii.binar.binarchapter6.helper

import android.content.Context
import android.os.Environment
import androidx.core.content.ContextCompat
import java.io.File


/**
 * Created by nurrahmanhaadii on 11,May,2022
 */

object Utils {

    val getUrl = "https://kotlinlang.org/assets/kotlin-media-kit.pdf"

    fun getUrl() = "https://kotlinlang.org/assets/kotlin-media-kit.pdf"

    // path folder app sesuai package name
    fun getRootDirPath(context: Context): String {
        return if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            val file: File = ContextCompat.getExternalFilesDirs(
                context.applicationContext,
                null
            )[0]
            file.absolutePath
        } else {
            context.applicationContext.filesDir.absolutePath
        }
    }
}