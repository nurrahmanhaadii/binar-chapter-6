package id.haadii.binar.binarchapter6.day5

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by nurrahmanhaadii on 10,May,2022
 */
class Day5ViewModel @Inject constructor(private val pref: CounterDataStoreManager) : ViewModel() {

    val newValue = MutableLiveData<Int>()
    val mediatorLiveData = MediatorLiveData<Int>()

    val counter = MutableLiveData<Int>()

    fun saveData(value: Int) {
        viewModelScope.launch {
            pref.setCounter(value)
        }
    }

    fun getData() {
        mediatorLiveData.addSource(pref.getCounter().asLiveData()) {
            mediatorLiveData.value = it
        }
    }

    fun getCounter() : LiveData<Int> {
        return pref.getCounter().asLiveData()
    }

    private fun incrementDataStore()  {
        viewModelScope.launch {
            pref.setIncrement()
        }
    }

    private fun decrementDataStore() {
        viewModelScope.launch {
            pref.setDecrement()
        }
    }

    fun increment() {
        newValue.value = newValue.value?.plus(1)

        // menyimpan value increment
        incrementDataStore()
    }

    fun decrement() {
        newValue.value = newValue.value?.minus(1)

        // menyimpan value decrement
        decrementDataStore()
    }

    fun saveImage(value: String) {
        viewModelScope.launch {
            pref.setImage(value)
        }
    }

    fun getImage() : LiveData<String> {
        return pref.getImage().asLiveData()
    }
}