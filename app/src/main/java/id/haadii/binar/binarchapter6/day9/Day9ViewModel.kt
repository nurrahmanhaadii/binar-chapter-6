package id.haadii.binar.binarchapter6.day9

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import id.haadii.binar.binarchapter6.model.Resource
import id.haadii.binar.binarchapter6.repository.MovieRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by nurrahmanhaadii on 14,May,2022
 */
class Day9ViewModel(private val repository: MovieRepository): ViewModel() {

    fun getMovie(key: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getMovie(key)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getData(key: String) = liveData(Dispatchers.IO) {
        // kondisi kyk ini bakal rentan error jika balikan data dari api bukan MovieResponse
        // jd butuh data class pembungkus utk menangani kondisi balikan error
        emit(repository.getMovie(key))
    }
}