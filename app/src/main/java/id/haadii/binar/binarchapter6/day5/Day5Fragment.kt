package id.haadii.binar.binarchapter6.day5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import id.haadii.binar.binarchapter6.App
import id.haadii.binar.binarchapter6.R
import id.haadii.binar.binarchapter6.base.BaseFragment
import id.haadii.binar.binarchapter6.databinding.FragmentDay5Binding
import id.haadii.binar.binarchapter6.viewModelsFactory
import org.koin.android.ext.android.inject
import javax.inject.Inject

class Day5Fragment : BaseFragment() {

    private var _binding: FragmentDay5Binding? = null
    private val binding get() = _binding!!

    private val pref: CounterDataStoreManager by inject()
    @Inject
    lateinit var prefD: CounterDataStoreManager
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: Day5ViewModel by viewModels { viewModelFactory }

    val isActived = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDay5Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewModel.getData()
        observeData()
        observeCounter()
        setValue()
        increment()
        decrement()
    }

    private fun observeData() {
        // tempat untuk update value pas pertama buka app
        viewModel.mediatorLiveData.observe(viewLifecycleOwner) {

        }

        // tempat untuk update value dari button plus dan button minus
        viewModel.newValue.observe(viewLifecycleOwner) {
            binding.tvValue.text = it.toString()
        }
    }

    private fun observeCounter() {
        // tempat untuk update value pas pertama buka app
        viewModel.getCounter().observe(viewLifecycleOwner) {
            binding.tvValue.text = it.toString()

            // update data new value yg nnti bakal di pake utk increment dan decrement
            viewModel.newValue.value = it
        }
    }

    private fun setValue() {
        binding.btnSet.setOnClickListener {
            val value = binding.tvValue.text.toString().toInt()
            viewModel.saveData(value)
        }
    }

    private fun increment() {
        binding.btnIncrease.setOnClickListener {
            viewModel.increment()
        }
    }

    private fun decrement() {
        binding.btnDecrease.setOnClickListener {
            viewModel.decrement()
        }
    }

}