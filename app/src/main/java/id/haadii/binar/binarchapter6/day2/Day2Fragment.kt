package id.haadii.binar.binarchapter6.day2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import id.haadii.binar.binarchapter6.MainActivity
import id.haadii.binar.binarchapter6.R

class Day2Fragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        IntentFilter("id.haadii.chapter6")
        return inflater.inflate(R.layout.fragment_day2, container, false)
    }

    override fun onResume() {
        super.onResume()
//        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(br, IntentFilter("id.haadii.chapter6"))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val intent = Intent(requireContext(), MyService::class.java)

        requireActivity().startService(intent)
//        requireActivity().stopService(intent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            requireContext().startForegroundService(intent)
        }
    }

    private val br = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null && p1.extras != null) {
                val message = p1.getStringExtra("message")
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

}