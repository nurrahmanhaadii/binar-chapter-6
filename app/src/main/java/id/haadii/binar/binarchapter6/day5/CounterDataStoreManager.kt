package id.haadii.binar.binarchapter6.day5

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


/**
 * Created by nurrahmanhaadii on 10,May,2022
 */
class CounterDataStoreManager(private val context: Context) {

    fun getCounter() : Flow<Int> {
        return context.dataStore.data.map { pref ->
            pref[COUNTER_KEY] ?: 0
        }
    }

    fun getBool() : Flow<Boolean> {
        return context.dataStore.data.map { pref ->
            pref[BOOL_KEY] ?: false
        }
    }

    fun getImage() : Flow<String> {
        return context.dataStore.data.map { pref ->
            pref[IMAGE_KEY] ?: ""
        }
    }

    suspend fun setIncrement() {
        context.dataStore.edit { pref ->
            // buat dapetin value yang sekarang
            val currentValue = pref[COUNTER_KEY]

            // jika current value tidak null
            currentValue?.let {
                pref[COUNTER_KEY] = it.plus(1)
            }
        }
    }

    suspend fun setDecrement() {
        context.dataStore.edit { pref ->
            // buat dapetin value yang sekarang
            val currentValue = pref[COUNTER_KEY]

            // jika current value tidak null
            currentValue?.let {
                pref[COUNTER_KEY] = it.minus(1)
            }
        }
    }

    suspend fun setCounter(value: Int) {
        context.dataStore.edit { pref ->
            pref[COUNTER_KEY] = value
            pref[BOOL_KEY] = true
        }
    }

    suspend fun setImage(value: String) {
        context.dataStore.edit { pref ->
            pref[IMAGE_KEY] = value
        }
    }

    companion object {
        private const val DATA_STORE_NAME = "counter_preference"
        private val COUNTER_KEY = intPreferencesKey("counter_key")
        private val BOOL_KEY = booleanPreferencesKey("bool_key")
        private val IMAGE_KEY = stringPreferencesKey("string_key")
        private val Context.dataStore by preferencesDataStore(name = DATA_STORE_NAME)
    }
}