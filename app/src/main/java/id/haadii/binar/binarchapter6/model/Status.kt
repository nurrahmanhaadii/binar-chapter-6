package id.haadii.binar.binarchapter6.model


/**
 * Created by nurrahmanhaadii on 14,May,2022
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
