package id.haadii.binar.binarchapter6

import android.content.Context
import javax.inject.Inject


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
class Name @Inject constructor(private val context: Context?, private val data: Data?) {

    fun getName() = "Hadi"

    fun getNameFromRes() = context?.getString(R.string.txt_name)

    fun getNameFromOtherDependencies() = data?.getDataName()
}