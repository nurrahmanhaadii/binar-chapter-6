package id.haadii.binar.binarchapter6.day9

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import id.haadii.binar.binarchapter6.databinding.FragmentDay9Binding
import id.haadii.binar.binarchapter6.model.Status
import id.haadii.binar.binarchapter6.repository.MovieRepository
import id.haadii.binar.binarchapter6.service.ApiClient
import id.haadii.binar.binarchapter6.service.ApiService
import id.haadii.binar.binarchapter6.viewModelsFactory
import org.koin.android.viewmodel.ext.android.viewModel


class Day9Fragment : Fragment() {
    private var _binding : FragmentDay9Binding? = null
    private val binding get() = _binding!!

    private val viewModel: Day9ViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay9Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeMovie()
    }

    private fun observeMovie() {
        viewModel.getMovie("498549d41b041783631ec3d45977d4c2").observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    // Handle ketika data loading
                    // progress bar muncul
                    binding.pb.isVisible = true
                }
                Status.SUCCESS -> {
                    // Handle ketika data success
                    // progress bar ilang
                    binding.pb.isVisible = false
                    it.data?.let { data ->
                        binding.tvText.text = data.results[0].title
                    }
                }
                Status.ERROR -> {
                    // Handle ketika data error
                    // progress bar ilang
                    binding.pb.isVisible = false
                    binding.tvText.text = it.message
                }
            }
        }
    }
}