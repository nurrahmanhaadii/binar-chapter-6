package id.haadii.binar.binarchapter6.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.haadii.binar.binarchapter6.day5.Day5Fragment


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun day5Fragment(): Day5Fragment
}