package id.haadii.binar.binarchapter6.day6

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import id.haadii.binar.binarchapter6.R
import id.haadii.binar.binarchapter6.databinding.FragmentVideoHandleBinding

class VideoHandleFragment : Fragment() {
    private var _binding : FragmentVideoHandleBinding? = null
    private val binding get() = _binding!!

    private var mPlayer: ExoPlayer? = null

    private val videoURL =
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentVideoHandleBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun initExoplayer() {
        // buat instance exoplayer
        mPlayer = ExoPlayer.Builder(requireContext()).build()

        // menghubungkan view dengan player
        binding.playerView.player = mPlayer

        // otomatis play ketika player udh siap
        mPlayer!!.playWhenReady = true

        mPlayer!!.apply {
            playWhenReady = true
            setMediaSource(createMediaSource())
        }
    }

    private fun createMediaSource(): MediaSource {
        // buat data source factory
        val dataSourceFactory = DefaultHttpDataSource.Factory()

        // create media item
        val mediaItem = MediaItem.fromUri(videoURL)

        // create media source
        val mediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(mediaItem)

        return mediaSource
    }

    private fun releaseExoPlayer() {
        if (mPlayer == null) {
            return
        }
        //release player when done
        mPlayer!!.release()
        mPlayer = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        initExoplayer()
    }

    override fun onResume() {
        super.onResume()
        initExoplayer()
    }

    override fun onPause() {
        super.onPause()
        releaseExoPlayer()
    }

    override fun onStop() {
        super.onStop()
        releaseExoPlayer()
    }

}