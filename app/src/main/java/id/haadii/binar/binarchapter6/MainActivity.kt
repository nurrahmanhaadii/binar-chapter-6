package id.haadii.binar.binarchapter6

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import dagger.android.AndroidInjection
import id.haadii.binar.binarchapter6.day2.TestReceiver
import id.haadii.binar.binarchapter6.day5.CounterDataStoreManager
import id.haadii.binar.binarchapter6.helper.StringHelper
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

//    // by lazy wajib val
//    private val name: Name by lazy { Name() }
//
//    // lateinit wajib var
//    private lateinit var name2: Name
//
//    // lgsg inisialisasi di awal
//    private val name3 = Name()

    // pake koin
    private val nameInject: Name by inject()
    // koin
    private lateinit var nameInject2: Name

    private val stringHelper: StringHelper by inject()

    @Inject
    lateinit var nameD: Name

    @Inject
    lateinit var dataStoreManager: CounterDataStoreManager

    @Inject
    lateinit var data: Data

    private val requestReceiveSMS = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)

        BuildConfig.FLAVOR
        BuildConfig.DEBUG

//        name2 = Name()
        nameInject2 = get()

        // mengecek permission pada device
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECEIVE_SMS), requestReceiveSMS)
        }

        val intentBroadcast = Intent("id.haadii.chapter6")
        intentBroadcast.putExtra("message", "adfdfadf")
        sendBroadcast(intentBroadcast)
        registerReceiver(TestReceiver(), IntentFilter("id.haadii.chapter6"))

        Toast.makeText(this, getName(), Toast.LENGTH_LONG).show()

        val a = stringHelper.isPositiveNumber(5)
    }

    private fun getName(): String {
        return nameD.getNameFromOtherDependencies() ?: "null"
    }

    private val br = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null && p1.extras != null) {
                val message = p1.getStringExtra("message")
                Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}