package id.haadii.binar.binarchapter6.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import id.haadii.binar.binarchapter6.Data
import id.haadii.binar.binarchapter6.Name
import id.haadii.binar.binarchapter6.day5.CounterDataStoreManager
import id.haadii.binar.binarchapter6.repository.MovieRepository
import javax.inject.Singleton


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideData(): Data {
        return Data()
    }

    @Singleton
    @Provides
    fun provideName(context: Application, data: Data): Name {
        return Name(context, data)
    }

    @Singleton
    @Provides
    fun provideDataStore(context: Application): CounterDataStoreManager = CounterDataStoreManager(context)
}