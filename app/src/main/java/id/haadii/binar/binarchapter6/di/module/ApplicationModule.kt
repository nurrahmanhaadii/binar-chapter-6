package id.haadii.binar.binarchapter6.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import id.haadii.binar.binarchapter6.Data
import id.haadii.binar.binarchapter6.Name
import id.haadii.binar.binarchapter6.day5.CounterDataStoreManager
import id.haadii.binar.binarchapter6.service.ApiClient
import javax.inject.Singleton


/**
 * Created by nurrahmanhaadii on 24,May,2022
 */
@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    @Singleton
    @Provides
    fun provideData(): Data = Data()

    @Provides
    fun provideName(context: Application, data: Data) : Name {
        return Name(context, data)
    }

    @Provides
    @Singleton
    fun provideDataStore(context: Application): CounterDataStoreManager {
        return CounterDataStoreManager(context)
    }

    @Provides
    @Singleton
    fun provideApiService() = ApiClient.instance
}