package id.haadii.binar.binarchapter6.day2

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat


/**
 * Created by nurrahmanhaadii on 26,April,2022
 */
class MyService : Service() {

    private var startMode: Int = 0             // indicates how to behave if the service is killed
    private var binder: IBinder? = null        // interface for clients that bind
    private var allowRebind: Boolean = false   // indicates whether onRebind should be used

    override fun onCreate() {
        // The service is being created
        Log.e("myservice", "oncreate")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // The service is starting, due to a call to startService()
        Log.e("myservice", "onStartCommand")

        val mBuilder = NotificationCompat.Builder(this, "001")
//        startForeground(123, mBuilder.build())
        return startMode
    }

    override fun onBind(intent: Intent): IBinder? {
        // A client is binding to the service with bindService()
        Log.e("myservice", "onBind")

        return binder
    }

    override fun onUnbind(intent: Intent): Boolean {
        // All clients have unbound with unbindService()
        Log.e("myservice", "onUnbind")

        return allowRebind
    }

    override fun onRebind(intent: Intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
        Log.e("myservice", "onRebind")

    }

    override fun onDestroy() {
        // The service is no longer used and is being destroyed
        Log.e("myservice", "onDestroy")

        stopSelf()
    }

}