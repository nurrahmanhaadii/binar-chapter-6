package id.haadii.binar.binarchapter6.day2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.widget.Toast

class SMSReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val extras = intent?.extras
        if (extras != null) {
            val sms = extras.get("pdus") as Array<*>
            for (i in sms.indices) {
                val format = extras.getString("format")

                val smsMessage =
                    SmsMessage.createFromPdu(sms[i] as ByteArray)

                val phoneNumber = smsMessage.originatingAddress
                val messageText = smsMessage.messageBody.toString()

                Toast.makeText(context, "$phoneNumber : $messageText", Toast.LENGTH_SHORT).show()
            }
        }
    }
}