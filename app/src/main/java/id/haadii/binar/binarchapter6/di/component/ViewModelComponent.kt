package id.haadii.binar.binarchapter6.di.component

import dagger.Component
import id.haadii.binar.binarchapter6.day5.Day5ViewModel
import id.haadii.binar.binarchapter6.di.module.ViewModelModule


/**
 * Created by nurrahmanhaadii on 25,May,2022
 */
@Component(
    modules = [ViewModelModule::class]
)
interface ViewModelComponent {
    fun inject(day5ViewModel: Day5ViewModel)
}