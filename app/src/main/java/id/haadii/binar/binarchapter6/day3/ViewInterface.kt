package id.haadii.binar.binarchapter6.day3


/**
 * Created by nurrahmanhaadii on 27,April,2022
 */
interface ViewInterface {
    fun showData(data: String)
}