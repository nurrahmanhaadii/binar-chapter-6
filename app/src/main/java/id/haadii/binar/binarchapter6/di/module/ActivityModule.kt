package id.haadii.binar.binarchapter6.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.haadii.binar.binarchapter6.MainActivity


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity
}