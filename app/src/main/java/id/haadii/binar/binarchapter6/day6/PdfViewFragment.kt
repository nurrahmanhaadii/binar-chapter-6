package id.haadii.binar.binarchapter6.day6

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import id.haadii.binar.binarchapter6.MainActivity
import id.haadii.binar.binarchapter6.databinding.FragmentPdfViewBinding
import id.haadii.binar.binarchapter6.helper.Utils
import java.io.File

class PdfViewFragment : Fragment() {
    private var _binding : FragmentPdfViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPdfViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openPdf()
    }

    private fun openPdf() {
        // dapeting data dari halaman sebelumnya
        val openFrom = arguments?.getInt("KEY_OPEN_FROM", 1)

        when (openFrom) {
            1 -> openFromAssets()
            2 -> openFromStorage()
            3 -> openFromInternet()
        }
    }

    // langsung buka pdf dari folder assets
    private fun openFromAssets() {
        binding.pdfView.fromAsset("kotlin-media-kit.pdf").load()
    }

    /**
     * 1. buka storage folder
     * 2. pilih file nya
     * 3. dapatkan uri
     * 4. open pdf dari uri
     */
    private fun openFromStorage() {
        Toast.makeText(requireContext(), "selectPDF", Toast.LENGTH_LONG).show()
        openStorage()
    }

    // buka storage folder hp
    private fun openStorage() {
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        val intent = Intent.createChooser(browseStorage, "Select PDF")
        resultLauncher.launch(intent)
    }

    // dapetin Uri
    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val uri = result.data!!.data
            showPdfFromUri(uri)
        }
    }

    // open pdf dari uri
    private fun showPdfFromUri(uri: Uri?) {
        binding.pdfView.fromUri(uri).load()
    }

    /**
     * 1. download filenya
     * 2. simpan di storage dengan nama file yg kita inginkan
     * 3. buka pdf dari file yg di download td
     */
    private fun openFromInternet() {
        binding.progressBar.visibility = View.VISIBLE
        val fileName = "myFile.pdf"
        downloadPdfFromInternet(
            Utils.getUrl,
            Utils.getRootDirPath(requireContext()),
            fileName
        )
    }

    // download file
    private fun downloadPdfFromInternet(url: String, dirPath: String, fileName: String) {
        PRDownloader.download(
            url,
            dirPath,
            fileName
        ).build()
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    Toast.makeText(requireContext(), "downloadComplete", Toast.LENGTH_LONG)
                        .show()
                    // dapeting file
                    val downloadedFile = File(dirPath, fileName)
                    binding.progressBar.visibility = View.GONE

                    // munculin pdf dari file
                    showPdfFromFile(downloadedFile)
                }

                override fun onError(error: com.downloader.Error?) {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        "Error in downloading file : $error",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            })
    }

    // munculin pdf dari file
    private fun showPdfFromFile(file: File) {
        binding.pdfView.fromFile(file)
            .password(null)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .onPageError { page, _ ->
                Toast.makeText(
                    requireContext(),
                    "Error at page: $page", Toast.LENGTH_LONG
                ).show()
            }
            .load()
    }
}