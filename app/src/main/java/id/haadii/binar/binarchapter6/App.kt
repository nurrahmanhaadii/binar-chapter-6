package id.haadii.binar.binarchapter6

import android.app.Application
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import id.haadii.binar.binarchapter6.day5.CounterDataStoreManager
import id.haadii.binar.binarchapter6.day9.Day9ViewModel
import id.haadii.binar.binarchapter6.di.component.ApplicationComponent
import id.haadii.binar.binarchapter6.di.component.DaggerApplicationComponent
import id.haadii.binar.binarchapter6.di.module.ApplicationModule
import id.haadii.binar.binarchapter6.helper.StringHelper
import id.haadii.binar.binarchapter6.repository.MovieRepository
import id.haadii.binar.binarchapter6.service.ApiClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import javax.inject.Inject


/**
 * Created by nurrahmanhaadii on 17,May,2022
 */
class App : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().application(this).build()
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        // Initialization Stetho
        Stetho.initialize(Stetho.newInitializerBuilder(this)
            .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
            .build()
        )
        // Init koin
        initKoin()
        appComponent.inject(this)
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, repoModule, viewModelModule))
        }
    }

    val repoModule = module {
        single { MovieRepository(get()) }
    }

    val viewModelModule = module {
        single { Day9ViewModel(get()) }
    }

}

val appModule = module {
    single { CounterDataStoreManager(androidContext()) }
    single { ApiClient.instance }
    single { Data() }
    single { StringHelper() }

    factory { Name(androidContext(), get()) }
}