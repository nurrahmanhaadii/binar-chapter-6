package id.haadii.binar.binarchapter6.day6

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import id.haadii.binar.binarchapter6.R
import id.haadii.binar.binarchapter6.databinding.FragmentWebViewBinding
import id.haadii.binar.binarchapter6.helper.Utils


class WebViewFragment : Fragment() {
    private var _binding : FragmentWebViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentWebViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.webView.apply {
            webViewClient = PdfWebClient()

            // buat mengaktifkan javascript di webview
            settings.setSupportZoom(true)
            settings.javaScriptEnabled = true

            val urlPdf = "https://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters.pdf"
            // utk load halaman web
            loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=$urlPdf")
        }
    }

    inner class PdfWebClient : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            binding.pb.isVisible = false
        }
    }

}