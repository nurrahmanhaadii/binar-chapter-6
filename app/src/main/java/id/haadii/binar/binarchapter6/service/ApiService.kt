package id.haadii.binar.binarchapter6.service

import id.haadii.binar.binarchapter6.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by nurrahmanhaadii on 14,May,2022
 */
interface ApiService {
    @GET("movie/popular")
    suspend fun getAllMovie(@Query("api_key") key: String) : MovieResponse
}