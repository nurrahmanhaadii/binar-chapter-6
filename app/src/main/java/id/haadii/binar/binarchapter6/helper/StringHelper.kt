package id.haadii.binar.binarchapter6.helper

import java.util.*


/**
 * Created by nurrahmanhaadii on 26,May,2022
 */
class StringHelper {

    fun isPositiveNumber(number: Int): Boolean {
        return number >= 0
    }

    fun toCapital(text: String): String {
        return text.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
    }

    fun doSomething(today: Date, someDate: Date): String? {
        return when {
            someDate.after(today) -> {
                "Tomorrow"
            }
            someDate.before(today) -> {
                "Yesterday"
            }
            else -> {
                "Today"
            }
        }
    }
}