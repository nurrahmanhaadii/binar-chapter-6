package id.haadii.binar.binarchapter6.repository

import id.haadii.binar.binarchapter6.service.ApiClient
import id.haadii.binar.binarchapter6.service.ApiService


/**
 * Created by nurrahmanhaadii on 14,May,2022
 */
class MovieRepository(private val apiService: ApiService) {
    suspend fun getMovie(key: String) = apiService.getAllMovie(key)
}