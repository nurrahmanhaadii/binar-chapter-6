package id.haadii.binar.binarchapter6.day6

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.haadii.binar.binarchapter6.BuildConfig
import id.haadii.binar.binarchapter6.R
import id.haadii.binar.binarchapter6.databinding.FragmentDay6Binding

class Day6Fragment : Fragment() {
    private var _binding : FragmentDay6Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay6Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openPdfHandle()
        openImageHandle()
        openVideoHandle()
        changeTitleBasedOnFlavor()
    }

    private fun changeTitleBasedOnFlavor() {
        val title = if (BuildConfig.FLAVOR == "staging") {
            "File Processing\nStaging"
        } else {
            "File Processing\nProduction"
        }

        binding.textView.text = title
    }

    private fun openPdfHandle() {
        binding.btnPdfHandle.setOnClickListener {
            findNavController().navigate(R.id.action_day6Fragment_to_pdfHandleFragment)
        }
    }

    private fun openImageHandle() {
        binding.btnImageHandle.setOnClickListener {
            findNavController().navigate(R.id.action_day6Fragment_to_imageHandleFragment)
        }
    }

    private fun openVideoHandle() {
        binding.btnVideoHandle.setOnClickListener {
            findNavController().navigate(R.id.action_day6Fragment_to_videoHandleFragment)
        }
    }

}