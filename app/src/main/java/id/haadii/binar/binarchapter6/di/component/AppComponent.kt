package id.haadii.binar.binarchapter6.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import id.haadii.binar.binarchapter6.App
import id.haadii.binar.binarchapter6.di.module.ActivityModule
import id.haadii.binar.binarchapter6.di.module.AppModule
import id.haadii.binar.binarchapter6.di.module.FragmentModule
import javax.inject.Singleton


/**
 * Created by nurrahmanhaadii on 23,May,2022
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ActivityModule::class,
        FragmentModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}